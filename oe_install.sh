version="v8"
install_directory="/opt/openerp"
#Update apt source list
apt-get update 
#Download and install updates
apt-get upgrade -y

#set locale ---------------------
#old
##locale-gen  fr_FR  fr_FR.UTF-8
##dpkg-reconfigure locales

#new solution
export LANGUAGE=fr_FR.UTF-8
export LANG=fr_FR.UTF-8
export LC_TYPE=fr_FR.UTF-8
locale-gen  fr_FR  fr_FR.UTF-8
dpkg-reconfigure locales
LC_ALL="fr_FR.utf8"
#export LC_ALL=fr_FR.UTF-8

##dpkg --configure -a #ADDED AFTER PB IN TEAMIT SEVER
#install git 
apt-get install git python-pip -y
#install required packages
apt-get install libgvc5  -y # v14 No
apt-get install graphviz ghostscript postgresql-client \
python-dateutil python-feedparser python-matplotlib \
python-ldap python-libxslt1 python-lxml python-mako \
python-openid python-psycopg2 python-pybabel python-pychart \
python-pydot python-pyparsing python-reportlab python-simplejson \
python-tz python-vatnumber python-vobject python-webdav \
python-werkzeug python-xlwt python-yaml python-imaging -y
#install some other packages that we will probably need in future
apt-get install gcc python-dev mc bzr python-setuptools python-babel \
python-feedparser python-reportlab-accel python-zsi python-openssl \
python-egenix-mxdatetime python-jinja2 python-unittest2 python-mock \
python-docutils lptools make python-psutil python-paramiko poppler-utils \
python-pdftools antiword postgresql -y

#ADDED
apt-get install python-requests -y
apt-get install python-decorator -y
apt-get install python-passlib -y

#------ install Webkit-----------
wget download.gna.org/wkhtmltopdf/0.12/0.12.0/wkhtmltox-linux-amd64_0.12.0-03c001d.tar.xz
tar -Jxvf wkhtmltox-linux-amd64_0.12.0-03c001d.tar.xz
cp wkhtmltox/bin/wkhtmltopdf /usr/bin/wkhtmltopdf

#set locale --------------------A VOIR ---
##locale-gen  fr_FR  fr_FR.UTF-8
##dpkg-reconfigure locales
#pour la versio 8
apt-get install python-pypdf python-psutil -y
apt-get install apache2 libapache2-mod-gnutls libapache2-mod-python libapache2-mod-wsgi -y
#install gdata client (since ubuntu package is old we download and install from source)
wget http://gdata-python-client.googlecode.com/files/gdata-2.0.17.tar.gz
tar zxvf gdata-2.0.17.tar.gz
cd gdata-2.0.17/
python setup.py install
#create a new openerp system user for openerp and other related processe
adduser --system --shell=/bin/bash --home=/opt/openerp --gecos 'OpenERP' --group openerp
#Create database user for openerp
cd ..
su - postgres -c "createuser -s openerp"
#move to the install directory
su - openerp -c "git clone -b 8.0 https://github.com/odoo/odoo.git"
#mkdir ${install_directory} deja cree
#get  odoo from git
su - openerp -c "mv odoo ${version}"
 
 
#create rep for others addons
#su - openerp -c "mkdir /opt/openerp/addons_tn"

#----------------Config--------------------------------------------------------------
#cp ${install_directory}/${version}/debian/openerp-server.conf /etc/openerp-server8.conf  
cp oe.conf /etc/openerp-server8.conf   
#Change the file permissions and file ownership to the openerp user.
chown openerp: /etc/openerp-server8.conf
chmod 640 /etc/openerp-server8.conf
#Log file
mkdir /var/log/openerp8  
mkdir /var/run/openerp8
chown openerp:openerp /var/log/openerp8
chown openerp:openerp /var/run/openerp8
chmod 777 /var/run/openerp8/

#Copy Logrotate file from source 
cp ${install_directory}/${version}/debian/logrotate  /etc/logrotate.d/openerp-server8
chmod 755 /etc/logrotate.d/openerp-server8

#----------------start automatically-------------------------------------
cp oe_start /etc/init.d/openerp-server8 
chmod +x /etc/init.d/openerp-server8
update-rc.d openerp-server8 defaults

#--------------------install gevent-------
apt-get install python-gevent -y
pip install psycogreen
cp /etc/openerp-server8.conf /etc/openerp-gevent8.conf
chown openerp: /etc/openerp-gevent8.conf
chmod 640 /etc/openerp-gevent8.conf
cp  oe_start_gevent  /etc/init.d/openerp-gevent8 
chmod +x /etc/init.d/openerp-gevent8
update-rc.d openerp-gevent8 defaults
 

