#!/bin/bash
DATE=`date +%Y-%m-%d_%H-%M-%N`   
directory="/opt/openerp/v8/backup/"
#mkdir ${directory}
#chown openerp:openerp ${directory}

hostname=localhost
db=db_name
filename="${directory}${hostname}_${db}_${DATE}.dump"
echo $DATE"------- Début backup ${filename} "  >> ${directory}"Log.txt"  

#su  postgres 
pg_dump  --username postgres -p 5432 -Fc  $db  -f  $filename
DATE=`date +%Y-%m-%d_%H-%M-%N`
echo $DATE"------- Fin backup ${filename} "  >> ${directory}"Log.txt"  



#https://openerp.my.openerp.com/forum/Help-1/question/How-to-setup-a-regular-PostgreSQL-database-backup-4728
#mkdir /var/scripts
#mkdir /opt/openerp/v8/backup/
#vi /etc/crontab
#00 13 * * * postgres /var/scripts/backup_postgres.sh
#00 01 * * * postgres /var/scripts/backup_postgres.sh


 

 
